\documentclass[a4paper,12pt,oneside,headsepline]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{scrlayer-scrpage}
\usepackage{titlesec}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage{tikz}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{changepage}
\pagestyle{scrheadings}
\ihead{Bilan technique Picasoft A20}
\ohead{\pagemark}
\chead{}
\cfoot{}

\hypersetup{%
	colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,%
	breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,%
	plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,%
	urlcolor=blue, linkcolor=blue, citecolor=blue
}

\begin{document}
	
		\vspace*{\fill}
	\begin{center}
		\textbf{{\Huge Bilan technique Picasoft  A20}}\\
		\vspace{2cm}
		\includegraphics[scale=.7]{logo.png}\\
		\vspace{1cm}
		{\Large 03 février 2020}
	\end{center}
	\vspace*{\fill}
	\clearpage
	
	\tableofcontents
	\newpage

\section{Introduction}

Ce document présente l’activité de l’association Picasoft sur le semestre d'automne 2020 (du 1er Août au 31 janvier). Il a pour but de donner un aperçu complet des actions menées par l’association et de son état courant.

Ce bilan ne détaille pas les tâches "classiques", à savoir :

\begin{itemize}
	\item Les mises à jour des services (ou ajouts de fonctionnalités...)
	\item Les migrations mineures (passage d'une base de donnée à une autre...)
	\item Les opérations de maintenance (renouveller des clés...)
\end{itemize}

\section{Guides pour l'équipe technique}

Le wiki reflète à présent l'ensemble de ce qui tourne en production. Mais même avec une description de l'infrastructure, il est difficile pour une nouvelle personne de savoir ce qu'il faut faire pour veiller à son bon fonctionnement.

Des tutoriels ont été rédigés pour aider les membres à vérifier que tout fonctionne bien, prévenir les problèmes avant qu'ils ne soient visibles et garder les systèmes à jour.

Un \href{https://wiki.picasoft.net/doku.php?id=technique:resume}{résumé technique}, un \href{https://wiki.picasoft.net/doku.php?id=technique:tips:premiers_pas}{guide d'arrivée} et un \href{https://wiki.picasoft.net/doku.php?id=technique:tips:todo_adminsys}{guide des tâches d'administration système} synthétise l'accès aux informations du wiki technique.

\section{Administration système }

\subsection{Monitoring}

Ce semestre a été l'occasion de revoir le fonctionnement de base de tout notre système de monitoring.

Historiquement Picasoft stockait les métriques des services (nombre de pads...) dans une base InfluxDB en s'appuyant sur \href{https://gitlab.utc.fr/picasoft/projets/picasoft-metrics-bot}{un petit bot en Python}. Les métriques des machines étaient, elles, récoltées en utilisant \href{https://prometheus.io/docs/introduction/overview/}{un serveur Prometheus} et des \href{https://github.com/prometheus/node_exporter}{\texttt{node-exporters}}. Les deux bases étaient ensuite consultées en utilisant Grafana.

Pour la nouvelle stack, nous avons mis en place Victoria Metrics pour remplacer InfluxDB et Prometheus. Cette solution est beaucoup plus performante que InfluxDB, et complètement compatible avec Prometheus. Grafana est toujours utilisé pour consulter les métriques.  

Une vue d'ensemble de cette stack se trouve \href{https://wiki.picasoft.net/doku.php?id=technique:adminsys:monitoring:metrologie:stack-picasoft}{sur le wiki}.

Les métriques de Wekan, de CodiMD et de Proxmox ont été ajoutées et sont visibles via un dashboard dédié.

\subsection{Suppression de la chaîne d'intégration}

La chaîne d'intégration est un système mis en place par la TX sécurité en automne 2018. Son objectif était d'automatiser la construction, l'analyse de sécurité et le déploiement des conteneurs Docker de nos services à chaque push sur un dépôt Gitlab dédié (\texttt{dockerfiles}).

Une première simplification a supprimé l'étape de déploiement, qui générait trop de frustrations et dépossédait les membres de l'équipe technique d'une vraie capacité de comprendre ce qu'il se passe et de résoudre les problèmes.

Finalement, l'étape qui restait (construction et analyse des images) a été jugée trop complexe, trop opaque et avec beaucoup trop de petits bugs dévitalisants.

La chaîne d'intégration a donc été totalement supprimée au profit d'opérations manuelles. 

Elle aura eu pour point positif d'uniformiser la structure du dépôt et de systématiser le versionnage des fichiers nécessaires à lancer n'importe lequel de nos services.

\section{Sécurité}

\subsection{Mises à jour automatiques}

Les failles de sécurité sont un risque majeur pour l’infrastructure. Une faille connue mais non patchée est dangereuse : dès qu’elle est rendue publique, des robots commencent à tenter de les exploiter sur les machines connectées à Internet.

Nos machines tournent sur Debian, une distribution Linux pensée pour la stabilité. Les mises à jour sont donc relativement rares, à l'exception de celles qui concernent la sécurité, distribuées via un canal dédié.

Nos machines reçoivent et appliquent automatiquement ces mises à jour, une fois par jour. Le risque d'instabilité est donc très légèrement accru pour un important bénéfice.

\subsection{Pare-feu}

Un pare-feu simple, n'autorisant l'accès qu'aux ports en liste blanche (exemple : DNS, ports du web), a été généralisé à l'ensemble des machines.

L'accès aux métriques des machines n'est autorisé qu'à la machine de \texttt{monitoring}.

\subsection{TLS}

L'accès à l'ensemble des services de Picasoft est maintenant sécurisé par TLS. Le dernier manquant était LDAP.

Le renouvellement des certificats TLS des services non-web (Mumble, mail, LDAP) est maintenant automatique.

\subsection{Ouverture du dépôt dockerfiles}

Le dépôt \texttt{dockerfiles}, qui contient l'ensemble des fichiers utilisés pour déployer nos services, a été rendu public par souci d'essaimage et de transparence.

Cette opération révèle des informations sur nos services (images Docker utilisées, paquets installés, configuration, etc) qui pourraient servir à un attaquant.

Ceci étant :
\begin{itemize}
	\item La sécurité par l'obscurité n'est clairement pas plus efficace
	\item Le caractère public du dépôt incite à faire attention et être rigoureux
\end{itemize}

\section{Infrastructure}

\subsection{Nouvelle machine virtuelle}

Une nouvelle machine, \texttt{media}, a été mise en place pour accueillir les services potentiellement consommateurs d'espace disque. Pour le moment, elle héberge Peertube et Lufi.

Les données sont stockées sur des disques durs, car le point de congestion le plus probable est le réseau, pas besoin de "gâcher" de la mémoire SSD.

\subsection{Mise à jour des hyperviseurs}

Alice et Bob, nos deux machines physiques, ont été mises à jour vers Proxmox 6, une version majeure basée sur Debian 10 et apportant de nombreuses améliorations, dont un nouveau format de backup.

\subsection{Achat de disques supplémentaires}

Sur Alice, les disques durs commençaient à être plein, en particulier à cause des backups des machines virtuelles hébergées sur Bob. De plus, le projet d'hébergement d'une machine virtuelle \texttt{multimedia} laissait présager un usage disque encore plus important.

Picasoft a pour ce faire acheté deux disques dur de 2To, montés en RAID1, installés sur Alice.

\subsection{Projet d'achat d'une nouvelle machine}

Le précédent bilan technique présentait une réflexion préliminaire sur l'achat d'une troisième machine.

Cette machine va être achetée au semestre prochain, grâce aux subventions du FSDIE obtenue ce semestre. 

L'idée est de l'héberger dans le datacenter associatif de Rhizome situé dans les locaux du Centre d'Innovation de l'UTC. Les objectifs sont les suivants :

\begin{itemize}
	\item Facilité d'intervention physique en cas de panne
	\item Renforcement du côté "local" de Picasoft
	\item Fiabilisation de l'infrastructure : en cas de panne à Toulouse, il nous reste une partie
	\item Échanges de sauvegardes : en l'état, si Toulouse brûle, toutes les données sont perdues
\end{itemize}

Une des idées évoquées est de rapatrier les services les plus utilisés à l'UTC sur les machines stockées à l'UTC, pour toutes les raisons évoquées ci-dessus.

Un plan d'action a été établi pour mettre en place un nouveau système de sauvegardes et de correctement les externaliser. Une fois la machine achetée, il sera rapidement mis en place.

\section{Incidents}

Deux incidents se sont produits ce semestre :

\begin{itemize}
	\item Le 10 octobre 2020, lors de la mise à jour vers Proxmox 6, Alice ne redémarre pas. Un bénévole de Tetaneutral redémarre la machine physiquement, tout se passe bien.
	\item Le 11 novembre 2020, un bug dans Docker entraîne la suppression d'une partie des données de production. Grâce aux backups, les données sont rapidement rétablies.
\end{itemize}

\end{document}
